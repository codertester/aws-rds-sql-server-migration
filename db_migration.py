# ******************************************************************************************************************
#
#  Copyright © 2020 - present. Codertester.  All Rights Reserved.
#
#  This file implements MigrateSQLServer() class for:
#
#    a) Connecting/disconnecting to SQL Server with "pymssql" connector/driver library.
#    b) Running queries against SQL Server cluster databases.
#    b) Migrating SQL Server to RDS SQL Server via Native Restore of files (.bak files) in S3 bucket.
#
#    Note:
#       a) Assuming Python v3.6 on Ubuntu 18.04 LTS, install pymssql as: sudo python3.6 -m pip install pymssql
#       b) GitHub repo of pymssql: https://github.com/pymssql/pymssql
# ******************************************************************************************************************

import sys
import pymssql
from pprint import pprint


class MigrateSQLServer():
    
    """
    A class for connecting to, querying and migrating SQL Server.
    """
      
    def __init__(self):
        print()
        print("Initiating RDS SQL Server Migration Engine...")
     # End  __init__() method
  
    def connect_db(self, server=None, user=None, password=None,  database=None, server_name=None, output_filename=None):
        try:
          connection = pymssql.connect(server=server, user=user, password=password, database=database, autocommit=True)
          print("{}{}{}{}{}{}{}".format("Connection to database (", database, ") is established on ", server_name, " (", server, ")"))
          print("Connection is okay ... ")
          return connection
        except(Exception) as mssql_connection_err:
          print("Error connecting to database: ", str(mssql_connection_err))
    # End connect_db() method
    
    def query_db(self, connection=None, mssql_query=None):
        cursor = connection.cursor()
        cursor.execute(mssql_query)
        output_data = cursor.fetchall()
        pprint(output_data)
    # End query_db() method
    
    def demarcation_lines_and_spaces(self):
        self.separator_line()
        self.separator_line()
        print("")
        print("")
    # End demarcation_lines_and_spaces() method
    
    def migrate_sql_server_db(self, connection=None, database_list=None, backup_path_list=None, recovery_option=None):
        print("")
        self.separator_line()
        print("Started database restore command:")
        self.separator_line()
        
        for index, path in enumerate(backup_path_list):
            if recovery_option == 0:
                mssql_query = "{}{}{}{}{}{}".format("exec msdb.dbo.rds_restore_database @restore_db_name=",
                                                     database_list[index], ",", "@s3_arn_to_restore_from='arn:aws:s3:::",
                                                     backup_path_list[index], "';"
                                                    )
            elif recovery_option == 1:
               mssql_query = "{}{}{}{}{}{}{}{}{}".format("exec msdb.dbo.rds_restore_database @restore_db_name=",
                                                     database_list[index], ",", "@s3_arn_to_restore_from='arn:aws:s3:::",
                                                     backup_path_list[index], ",", "@with_norecovery=", recovery_option, "';"
                                                    )
                                                    
            self.query_db(connection=connection, mssql_query=mssql_query)
            print("")
            self.separator_line()
        print("End of database restore command.")
        self.separator_line()
        print("")
    # End migrate_sql_server_db() method
    
    def check_db_server_status(self, connection=None):
        print("")
        self.separator_line()
        print("Checking database server status:")
        mssql_query = "exec msdb..rds_task_status"
        self.query_db(connection=connection, mssql_query=mssql_query)
        print("Finished checking database server status.")
        self.separator_line()
        print("")
    # End check_db_server_statusb() method
        
    def get_credentials(self):
        return {
            "user" : "my_username",
            "password" : "my_password",
            "database_name" : "master"
        }
    # End get_credentials() method
    
    def separator_line(self):
        print("===========================================================================")
    # End separator_line() method
# End MigrateSQLServer() class

def main():
    """
    main function: app entry
    """
    # 0. define variables
    access_mssql = MigrateSQLServer()
    migrate = False
    # recovery_option should be: 0 (online after restore) or 1 (keeps in RESTORING state after restore)
    recovery_option = 0
    check_server_status = False
    redirect_stdout_to_file = True
    output_filename = "db_clusters_details.txt"
    database_list = ["db_name_one", "db_name_two", "db_name_et_all"]
                      
    # restore database .bak file: path of backup files list in S3:
    # each file corresponds to "full single file" backup of each database in the "database_list" above, respectively
    backup_path_list = [ "path_to/db_name_one_backup.bak", "path_to/db_name_two_backup.bak", "path_to/db_name_et_all_backup.bak"]
                        
    # 1. print to console (or redirecting) all stdout to output_filename, in append ('a') mode, note: file is located in CWD
    print("........")
    print(" ")
    if redirect_stdout_to_file:
        sys.stdout = open(output_filename , 'a')

    # 2. define dictionary to hold key-value pairs of db server(s) and endpoint/server-url/ip
    servers_dict = {
        "RDS-SQL-Server" : "rds_server_url_or_ip"
    }
    
    # 3. show/print all available db servers
    access_mssql.separator_line()
    print("CONNECTING TO THE FOLLOWING DATABASE SERVER(S): ")
    for item in servers_dict:
        print("Database Server Name  : {}".format(item))
        print("URL or IP or Endoint  : {}".format(servers_dict[item]))
    access_mssql.separator_line()
    print("")
    print("")
    print("Start Migration ...")
    
    # 4. connect,  migratrate, and check server status (if desired)
    server_count = 0
    for item in servers_dict:
        # a. define credentials
        server = servers_dict[item]
        server_name = item
        user = access_mssql.get_credentials().get("user")
        password = access_mssql.get_credentials().get("password")
        database = access_mssql.get_credentials().get("database_name")
        
        # b. update server count
        server_count = server_count  + 1
        print("(", server_count , ")")
        print("=====")
        
        # c. connect to server(s)
        connection = access_mssql.connect_db(server=server, user=user, password=password, database=database,
                                             server_name=server_name, output_filename=output_filename)
        
        # d. migrate/restore databases .bak file
        if migrate:
            access_mssql.migrate_sql_server_db(connection=connection, database_list=database_list, backup_path_list=backup_path_list)
            access_mssql.demarcation_lines_and_spaces()
            
        # e. check db server status
        if check_server_status:
            access_mssql.check_db_server_status(connection=connection)
            access_mssql.demarcation_lines_and_spaces()
# End main() method

# invoke app
if __name__ in ('__main__', 'app'):
    main()