# *********************************************************************************************************************************
#
#  Copyright © 2020 - present. Codertester.  All Rights Reserved.
#
#  This file implements StagingSQLServerCreate() class for:
#   1) The creating/restoring compononent of the nightly refresh staging of Primary (Master) RDS SQL Server using boto3 library.
#      It also sends an alert e-mail if corestaging instance is not created or missing.
#
# *********************************************************************************************************************************


import boto3
from datetime import datetime
from operator import itemgetter
from botocore.exceptions import ClientError


class StagingSQLServerCreate():
    
    """
    A class for create/restore compononent of nightly refresh staging of SQL Server.
    """
      
    def __init__(self, region_name=None):
        print()
        print("Initiating create/restore compononent of RDS SQL Server Staging ...")
        self.client_rds = boto3.client('rds')
        self.client_ses = boto3.client('ses', region_name=region_name)
        self.region_name = region_name
    # End  __init__() method
    
    def check_if_instance_exists(self, instance_identifier=None):
        try:
            response = self.client_rds.describe_db_instances(DBInstanceIdentifier=instance_identifier)
            return response.get('DBInstances')[0].get('DBInstanceIdentifier')
        except(ClientError) as client_error:
           print(client_error)
    # End check_if_instance_exists() method
    
    def get_latest_automated_snapshot_identifier(self, instance_identifier=None):
            response = self.client_rds.describe_db_snapshots(DBInstanceIdentifier=instance_identifier, SnapshotType="automated")
            snapshot_created_time_list = []
            snapshot_identifier_list = []
            # get all snapshots and dates created and append to separate lists
            all_snapshots = response.get("DBSnapshots")
            if all_snapshots:
                for index, item in enumerate(all_snapshots):
                    snapshot_created_time = all_snapshots[index].get('SnapshotCreateTime')
                    snapshot_created_time_list.append(snapshot_created_time)
                    snapshot_identifier = all_snapshots[index].get('DBSnapshotIdentifier')
                    snapshot_identifier_list.append(snapshot_identifier)
                # get time of latest automated snapshot from "snapshot_created_time_list" and its index in the list
                index, time_value = max(enumerate(snapshot_created_time_list), key=itemgetter(1))
                latest_automated_snapshot_time = time_value
                # then use the index to get the lastest automated snapshot identifier
                latest_automated_snapshot_identifier = snapshot_identifier_list[index]
                # print result to check and return the identifier
                print("-------------------------------------------------------------")
                print("latest_automated_snapshot_time: ", latest_automated_snapshot_time)
                print("latest_automated_snapshot_identifier: ", latest_automated_snapshot_identifier)
                print("-------------------------------------------------------------")
                return latest_automated_snapshot_identifier
            else:
                print("-------------------------------------------------------------")
                print("There is no snapshot for the specified instance identifier...")
                print("-------------------------------------------------------------")
    # End get_lastest_automated_snapshot_identifier() method
    
    def create(self, db_snapshot_identifier=None, source_instance_identifier=None, target_instance_identifier=None,
               db_subnet_group_name=None, sender=None, recipient=None, db_instance_class=None, port=None, availability_zone=None,
               option_group_name=None, vpc_security_group_ids=None, domain=None, domain_iam_role_name=None, iops=None):
        # then create a new instance based on lastest automated snapshot identifier
        response = self.client_rds.restore_db_instance_from_db_snapshot(
                DBInstanceIdentifier=target_instance_identifier,
                DBSnapshotIdentifier=db_snapshot_identifier,
                DBInstanceClass=db_instance_class,
                DBSubnetGroupName=db_subnet_group_name,
                Port=port,
                AvailabilityZone=availability_zone,
                MultiAZ=False,
                PubliclyAccessible=True,
                AutoMinorVersionUpgrade=True,
                OptionGroupName=option_group_name,
                Tags=[{'Key': 'Name', 'Value': target_instance_identifier}],
                Iops=iops,
                StorageType='io1',
                VpcSecurityGroupIds=vpc_security_group_ids,
                Domain=domain
                DomainIAMRoleName=domain_iam_role_name,
                CopyTagsToSnapshot=True,
                UseDefaultProcessorFeatures=True,
                DeletionProtection=False
        )
    # End create() method
    
    def restore_instance_from_snapshot(self, source_instance_identifier=None, target_instance_identifier=None,
                                       db_subnet_group_name=None, sender=None, recipient=None, db_instance_class=None, port=None, availability_zone=None,
                                       option_group_name=None, vpc_security_group_ids=None, domain=None, domain_iam_role_name=None, iops=None):
        # confirm target instance identifier is specified, and that instance with the same identifier does not exit
        instance_exist = self.check_if_instance_exists(instance_identifier=target_instance_identifier)
        confirm = (target_instance_identifier and not instance_exist)

        if confirm:
            # then get reference to lastest automated snapshot (snapshot of source instance)
            db_snapshot_identifier = self.get_latest_automated_snapshot_identifier(instance_identifier=source_instance_identifier)

            if db_snapshot_identifier:
                try:
                    # create coretstaging instance if it does not exist within the specified range of hours
                    self.create(db_snapshot_identifier=db_snapshot_identifier, source_instance_identifier=source_instance_identifier,
                                target_instance_identifier=target_instance_identifier, db_subnet_group_name=db_subnet_group_name,
                                sender=sender, recipient=recipient, db_instance_class=db_instance_class, port=port, availability_zone=availability_zone,
                                option_group_name=option_group_name, vpc_security_group_ids=vpc_security_group_ids, domain=domain,
                                domain_iam_role_name=domain_iam_role_name, iops=iops
                                )
                except(Exception) as restore_db_instance_error:
                    print("")
                    self.compose_and_send_corestaging_error_email(target_instance_identifier=target_instance_identifier, sender=sender, recipient=recipient)
                    print(restore_db_instance_error)
        else:
            print("Cannot create instance: An instance with the specified instance_identifier already exist.")
    # End of restore_instance_from_snapshot() method
    
    def compose_and_send_corestaging_error_email(self, target_instance_identifier=None, sender=None, recipient=None):
        region_name =  self.region_name
        subject = "Corestaging Instance: Not Created"
        body_text = "{}{}{}{}".format("This is an alert message to let you know that ", target_instance_identifier,  " is missing or not created today: ", datetime.utcnow().date())
        charset = "UTF-8"
        
        try:
            print("sending-mail")
            response = self.client_ses.send_email(
                Destination={'ToAddresses': [recipient]},
                Message={ 'Body': {'Text': {'Charset': charset,'Data': body_text}}, 'Subject': {'Charset': charset, 'Data': subject} },
                Source=sender
            )
            
            print(response)
        except(ClientError) as mail_error:
            print(mail_error.response['Error']['Message'])
        else:
            print("Email sent! Message ID:",  response['MessageId'])
    # End compose_and_send_corestaging_error_email() method

    def send_email_if_corestaging_instance_is_missing(self, target_instance_identifier=None, sender=None, recipient=None):
        instance_exist = self.check_if_instance_exists(instance_identifier=target_instance_identifier)
        confirm = (target_instance_identifier and not instance_exist)
        print(confirm)
        if confirm:
            self.compose_and_send_corestaging_error_email(target_instance_identifier=target_instance_identifier, sender=sender, recipient=recipient)
    # End send_email_if_corestaging_instance_is_missing() method
# End StagingSQLServerCreate() class


def main():
    """
    main function: app entry
    """
    # 1. define main variables
    region_name = "region_name" # "us-west-2" # aws region for Amazon SES
    email_sender = "db_messages@domain_name.com <db_messages@domain_name.com>"
    email_recipient = "db_messages_receiver@domain_name.com"
    source_instance_identifier = "name_of_primary_or_master_server"
    staging = StagingSQLServerCreate(region_name=region_name)
    restore = False
    db_subnet_group_name = "db_subnet_group_name"
    db_instance_class = 'db_instance_class' # "db.z1d.xlarge",
    port = "port-value" # 1433
    availability_zone = 'availability_zone' #'us-west-2b',
    option_group_name = 'option_group_name',
    vpc_security_group_ids = ['sg-value-1', 'sg-value-2', 'sg-value-n'],
    domain = 'domain_value',
    domain_iam_role_name = 'domain_iam_role_name',
    iops = "iops_value" #10000,
    
    # get and print date, hours and minutes
    utc_now = datetime.utcnow()
    hour = utc_now.hour
    minute = utc_now.minute
    print("------------------------------------------")
    print("utc_now: ", utc_now)
    print("hour: ", hour)
    print("minute: ", minute)
    print("------------------------------------------")
    # variables for instance creation
    
    # 2. get reference to the correct instance to be created: only a maximum of one corestaging instance should exist at this point in time
    instance_identifier_one = "corestaging-1"
    instance_identifier_two = "corestaging-2"
    instance_identifier = instance_identifier_one # default value
    if staging.check_if_instance_exists(instance_identifier=instance_identifier_one):
        instance_identifier = instance_identifier_two
        
   
    # 3. re-create corestaging, if it does not exit by 12 UTC (i.e. 4 AM PST)
    #    note: lamda is only invoked at scheduled time (12 UTC/4 AM PST) for this action to occur
    # confirm no corestaging  exit
    exist_corestaging_one = staging.check_if_instance_exists(instance_identifier=instance_identifier_one)
    exist_corestaging_two = staging.check_if_instance_exists(instance_identifier=instance_identifier_two)
    confirm = False
    if not exist_corestaging_one and not exist_corestaging_two:
        confirm = True
    
    if hour == 12 and confirm:
        restore = True
    
        # create/restore instance based on cloud event rule scheduled time
        if restore:
            print("")
            print("Restoring corestaging SQL Server ...")
            target_instance_identifier = instance_identifier
            staging.restore_instance_from_snapshot(source_instance_identifier=source_instance_identifier,
                                                   target_instance_identifier=target_instance_identifier,
                                                   db_subnet_group_name=db_subnet_group_name, sender=email_sender,
                                                   recipient=email_recipient, db_instance_class=db_instance_class,
                                                   port=port, availability_zone=availability_zone,
                                                   option_group_name=option_group_name, vpc_security_group_ids=vpc_security_group_ids,
                                                   domain=domain, domain_iam_role_name=domain_iam_role_name, iops=iops)
            print("target_instance_identifier: ", target_instance_identifier)
            print("source_instance_identifier: ", source_instance_identifier)
            print(" ")
            return
    
    # 4. ensure create/restore events (after delete) for staging: occur only within a time (utc hours) range, and then create
    #    note: lamda is only invoked based on cloud event rule pattern  for this action to occur
    if hour >= 8 and hour <= 11:
        restore = True
    
        # create/restore instance based on cloud event rule pattern
        if restore:
            print("")
            print("Restoring corestaging SQL Server ...")
            target_instance_identifier = instance_identifier
            staging.restore_instance_from_snapshot(source_instance_identifier=source_instance_identifier,
                                                   target_instance_identifier=target_instance_identifier,
                                                   db_subnet_group_name=db_subnet_group_name, sender=email_sender,
                                                   recipient=email_recipient, db_instance_class=db_instance_class,
                                                   port=port, availability_zone=availability_zone,
                                                   option_group_name=option_group_name, vpc_security_group_ids=vpc_security_group_ids,
                                                   domain=domain, domain_iam_role_name=domain_iam_role_name, iops=iops)
            print("target_instance_identifier: ", target_instance_identifier)
            print("source_instance_identifier: ", source_instance_identifier)
            print(" ")
# End main() method


def lambda_handler(event=None, context=None):
    main()
    return "Lamda function is successfully is invoked."