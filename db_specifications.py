# *******************************************************************************************************************
#
#  Copyright © 2020 - present. Codertester.  All Rights Reserved.
#
#  This file implements SpecifySQLServer() class for:
#
#    a) Connecting/disconnecting to SQL Server with "pymssql" connector/driver library.
#    b) Running queries against SQL Server cluster(s) to obtain details, including:
#       i)  Names and sizes of databases.
#       ii) Cluster cpu, memory and disk specifications.
#
#    Note:
#       a) Assuming Python v3.6, pymssql can be installed as: sudo python3.6 -m pip install pymssql
#       b) GitHub repo of pymssql: https://github.com/pymssql/pymssql
# *******************************************************************************************************************

import sys
import pymssql
from pprint import pprint

class SpecifySQLServer():
    
    """
    A class for connecting to and quering SQL Server.
    """
      
    def __init__(self, redirect_stdout_to_file=None, output_filename=None):
        print()
        print("Initiating SpecifySQLServer Engine...")
     # End  __init__() method
  
    def connect_db(self, server=None, user=None, password=None,  database=None, server_name=None, output_filename=None):
        "A method for initiating database connection"
        try:
          connection = pymssql.connect(server=server, user=user, password=password, database=database, autocommit=True)
          print("{}{}{}{}{}{}{}{}".format("Connection to database (", database, ") is established on ", server_name, " (", server, ")", " Server."))
          print("Connection is okay ... ")
          return connection
        except(Exception) as mssql_connection_err:
          print("Error connecting to database: ", str(mssql_connection_err))
    # End  connect_db() method
    
    def sql_server_cluster_details(self, connection=None):
        # 0. line
        self.separator_line()
        # 1. Database Names
        print("Database Names:")
        mssql_query = "USE master; SELECT name FROM sys.databases;"
        self.query_db(connection=connection, mssql_query=mssql_query)
        self.separator_line()
        
        # 2. Database Details
        mssql_query = """SELECT  sys.databases.name,  CONVERT(VARCHAR, (SUM(size)*8.0/1024000) ) + ' GB' AS [Total Disk Space]
                         FROM sys.databases
                         JOIN sys.master_files
                         ON  sys.databases.database_id=sys.master_files.database_id
                         GROUP BY sys.databases.name
                         ORDER BY sys.databases.name
                      """
        print("Details of SQL Server Cluster Database Sizes:")
        self.query_db(connection=connection, mssql_query=mssql_query)
        self.separator_line()
        
         # 3. SQL Server Version
        print("SQL Server Version:")
        mssql_query = "SELECT @@VERSION AS 'SQL Server Version';"
        self.query_db(connection=connection, mssql_query=mssql_query)
        self.separator_line()
        
        # 4. OS Version
        print("Windows OS Release Version:")
        mssql_query = "SELECT windows_release FROM sys.dm_os_windows_info;"
        self.query_db(connection=connection, mssql_query=mssql_query)
        self.separator_line()
        
        # 5. CPU Count
        print("Number of Logical CPUs on the System:")
        mssql_query = "SELECT cpu_count FROM sys.dm_os_sys_info;"
        self.query_db(connection=connection, mssql_query=mssql_query)
        self.separator_line()
        
        # 6. Memory
        print("Total Physical Memory (GB):")
        mssql_query = "SELECT physical_memory_kb/1000000 FROM sys.dm_os_sys_info;"
        self.query_db(connection=connection, mssql_query=mssql_query)
        self.separator_line()
        
        # 7. Disk Size
        print("Disk Size: DRIVE, TOTAL (GB) and FREE (GB)")
        mssql_query = """SELECT DISTINCT(volume_mount_point), total_bytes/1E+9 as Size_in_GB, available_bytes/1E+9 as Free_in_GB
                         FROM sys.master_files AS f CROSS APPLY sys.dm_os_volume_stats(f.database_id, f.file_id)
                         GROUP BY volume_mount_point, total_bytes/1E+9, available_bytes/1E+9
                         ORDER BY 1
                      """
        self.query_db(connection=connection, mssql_query=mssql_query)
        self.separator_line()
        
        # 8. VM Description
        print("Virtual Machine Type Description:")
        mssql_query = "SELECT virtual_machine_type_desc FROM sys.dm_os_sys_info;"
        self.query_db(connection=connection, mssql_query=mssql_query)
        self.separator_line()
    # End  sql_server_cluster_details() method
    
    def query_db(self, connection=None, mssql_query=None):
        cursor = connection.cursor()
        cursor.execute(mssql_query)
        output_data = cursor.fetchall()
        pprint(output_data)
        return output_data
    # End query_db() method
    
    def separator_line(self):
        print("===========================================================================")
    # End separator_line() method
    
    def demarcation_lines_and_spaces(self):
        self.separator_line()
        self.separator_line()
        print("")
        print("")
    # End demarcation_lines_and_spaces() method
        
    def get_credentials(self):
        return {
            "user" : "my_username",
            "password" :  "my_password",
            "database_name" : "master"
        }
    # End get_credentials() method
        
# End SpecifySQLServer() class

def main():
    """
    main function: app entry
    """
    # 0. create instance of AccessSQLServe() class
    redirect_stdout_to_file = False
    output_filename = "db_clusters_details.txt"
    access_mssql = SpecifySQLServer(redirect_stdout_to_file=redirect_stdout_to_file)
    stat = []
    
    # 1. print to console (or redirecting) all stdout to output_filename, in append ('a') mode
    #    note: file is located in current working directory
    print("Collecting and writing clusters' details to file in the CWD: In Progess.....")
    print("........")
    print(" ")
    if redirect_stdout_to_file:
        sys.stdout = open(output_filename , 'a')

    # 2. define dictionary to hold key-value pairs of db servers and ip adresses
    # a. all
    servers_dict = {
        "server_name_1": "server_name_1_ip_or_url",
        "server_name_2": "server_name_2_ip_or_url",
        "server_name_3": "server_name_3_ip_or_url",
        "server_name_n": "server_name_n_ip_or_url"
    }
    
    # 3. show/print all available db servers
    access_mssql.separator_line()
    print("CONNECTING TO THE FOLLOWING DATABASE SERVERS: ")
    for item in servers_dict:
        print("Database Server Name: {} , IP Value : {}".format(item, servers_dict[item]))
    access_mssql.separator_line()
    print("")
    print("")
    
    # 4. connect and get relevant information/statistics
    server_count = 0
    for item in servers_dict:
        # a. define credentials
        server = servers_dict[item]
        server_name = item
        # assumed same username and password for all database servers
        user = access_mssql.get_credentials().get("user")
        password = access_mssql.get_credentials().get("password")
        database = access_mssql.get_credentials().get("database_name")
        # b. update server count
        server_count = server_count  + 1
        print("(", server_count , ")")
        print("=====")
        # c. connect to server(s) and get relevant sql server details
        connection = access_mssql.connect_db(server=server, user=user, password=password, database=database,
                                             server_name=server_name, output_filename=output_filename)
        # d. get and show/print sql server details: database-name-sizes and cpu-mem-disk-specs
        access_mssql.sql_server_cluster_details(connection=connection)
        access_mssql.demarcation_lines_and_spaces()
    
# End main() method

# invoke app
if __name__ in ('__main__', 'app'):
    main()