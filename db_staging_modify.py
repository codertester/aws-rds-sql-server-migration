# *************************************************************************************************
#
#  Copyright © 2020 - present. Codertester.  All Rights Reserved.
#
#  This file implements ModifySQLServer() class to:
#   1) Modify/Disable "autobackup/snapshot"
#   2) Modify/Enable "performance insights" and
#   3) Make modifications in (1) and (2) above to be effective immediately
#   4) Modify/Enable CloudWatch logs export: change is appplied immediately by default
#   5) Modify/Update Route53 with 'A' and 'CNAME' values
#   6) Modify/Stop the stalled "restoring state" of any "stalled_db_name" database, if required
#   7) Delete previous day instance
#
#   All modifications are made on "corestaging" SQL Server using boto3 library,
#   after a successful restore of the lastest autobackup/snapshot as "corestaging" instance
# *************************************************************************************************


import boto3
import base64
import pymssql
from time import sleep
from json import loads
from pprint import pprint
from operator import itemgetter
from socket import gethostbyname
from boto3.session import Session
from botocore.exceptions import ClientError

   
class ModifySQLServer():
    
    """
    A class for modifying corestaging SQL Server after restore from auto-backup
    """
      
    def __init__(self, redirect_stdout_to_file=None):
        print()
        print("Modifying Corestaging RDS SQL Server ...")
        self.client_rds = boto3.client('rds')
        self.client_dns = boto3.client('route53')
    # End  __init__() method

    def check_if_instance_exists(self, instance_identifier=None):
        try:
            response = self.client_rds.describe_db_instances(DBInstanceIdentifier=instance_identifier)
            return response.get('DBInstances')[0].get('DBInstanceIdentifier')
        except(ClientError) as client_error:
            print(client_error)
    # End check_if_instance_exists() method
    
    def delete_instance(self, instance_identifier=None):
        if instance_identifier:
            try:
                response = self.client_rds.delete_db_instance(DBInstanceIdentifier=instance_identifier, SkipFinalSnapshot=True, DeleteAutomatedBackups=True)
            except(Exception) as delete_db_instance_error:
                print("")
                print(delete_db_instance_error)
    # End delete_instance() method
    
    def get_latest_or_oldest_created_instance_identifier(self, instance_identifier_one=None, instance_identifier_two=None, return_option=None):
        instance_identifier_list = []
        instance_created_time_list = []
        
        try:
            
            if self.check_if_instance_exists(instance_identifier=instance_identifier_one):
                instance_one = self.client_rds.describe_db_instances(DBInstanceIdentifier=instance_identifier_one)
                instance_identifier_one_time_created = instance_one.get('DBInstances')[0].get('InstanceCreateTime')
                instance_identifier_list.append(instance_identifier_one)
                instance_created_time_list.append(instance_identifier_one_time_created)
            
            if self.check_if_instance_exists(instance_identifier=instance_identifier_two):
                instance_two = self.client_rds.describe_db_instances(DBInstanceIdentifier=instance_identifier_two)
                instance_identifier_two_time_created = instance_two.get('DBInstances')[0].get('InstanceCreateTime')
                instance_identifier_list.append(instance_identifier_two)
                instance_created_time_list.append(instance_identifier_two_time_created)
            
            # get time of oldest created instance from "instance_created_time_list" and its index in the list
            index, time_value = min(enumerate(instance_created_time_list), key=itemgetter(1))
            oldest_instance_created_time = time_value
            index_latest, time_value_lastest = max(enumerate(instance_created_time_list), key=itemgetter(1))
            latest_instance_created_time = time_value_lastest
            # then use the index to get the oldest created instance identifier
            oldest_instance_identifier = instance_identifier_list[index]
            latest_instance_identifier = instance_identifier_list[index_latest]
            
            # print results to check, when under development (i.e. not in production)
            in_production = True
            if not in_production:
                print()
                print("instance_identifier_one_time_created:", instance_identifier_one_time_created)
                print("instance_identifier_two_time_created:", instance_identifier_two_time_created)
                print("-------------------------------------------------------------")
                print("oldest_instance_created_time: ", oldest_instance_created_time)
                print("oldest_instance_identifier: ", oldest_instance_identifier)
                print("latest_instance_created_time: ", latest_instance_created_time)
                print("latest_instance_identifier: ", latest_instance_identifier)
                print("-------------------------------------------------------------")
            
            #return the identifier
            if return_option == "oldest":
                return oldest_instance_identifier
            if return_option == "latest":
                return latest_instance_identifier
    
        except(ClientError) as client_error:
            print(client_error)
    # End get_latest_or_oldest_created_instance_identifier() method
    
    def modify_corestaging_instance_after_restore(self, target_instance_identifier=None, key_id=None):
        response = self.client_rds.modify_db_instance(
            DBInstanceIdentifier=target_instance_identifier,
            # 1. disable backup on the target_instance_identifier (i.e. "corestaging")
            BackupRetentionPeriod=0,
            # 2. enable performance insight on the target_instance_identifier (i.e. "corestaging") with 7 days retention period
            EnablePerformanceInsights=True,
            PerformanceInsightsKMSKeyId=key_id,
            # 3. make changes in (1) and (2) effective immediately
            ApplyImmediately=True,
            # 4. enable CloudWatch logs export: change is appplied immediately by default
            CloudwatchLogsExportConfiguration={'EnableLogTypes': ['agent', 'error']}
        )
    # End modify_corestaging_instance_after_restore() method
    
    def update_corestaging_private_ip_address_and_cname(self, a_name=None, dns_name=None, hosted_zone_id=None, ttl=None, endpoint=None, check_ip_details=None, cname_names=None):
        private_ip_address = str(gethostbyname(endpoint))
        
        if check_ip_details:
            print()
            print("Endpoint: ",  endpoint)
            print("IP Address: ", private_ip_address)
            print()
        
        response_one = self.client_dns.change_resource_record_sets(
            HostedZoneId=hosted_zone_id,
            ChangeBatch={
                'Comment': 'Updating Route53 with "A" and "CNAME" values',
                'Changes':
                 [
                    {
                        'Action': 'UPSERT',
                        'ResourceRecordSet':
                        {
                            'Name': a_name,
                            'Type': 'A',
                            'ResourceRecords': [{'Value': private_ip_address}],
                            'TTL': ttl
                        }
                    },
                    {
                        'Action': 'UPSERT',
                        'ResourceRecordSet':
                        {
                            'Name': cname_names[0],
                            'Type': 'CNAME',
                            'ResourceRecords': [{'Value': endpoint}],
                            'TTL': ttl
                        }
                    },
                    {
                        'Action': 'UPSERT',
                        'ResourceRecordSet':
                        {
                            'Name': cname_names[1],
                            'Type': 'CNAME',
                            'ResourceRecords': [{'Value': endpoint}],
                            'TTL': ttl
                        }
                    }
                ]
            }
        )
        
        print('Successfully updated "A" value of  "corestagingip.contentad.internal" record as private ip: ', private_ip_address)
        print()
    # End update_corestaging_private_ip_address() method
    
    def connect_db(self, server=None, user=None, password=None,  database=None, server_name=None):
        try:
          connection = pymssql.connect(server=server, user=user, password=password, database=database, autocommit=True)
          print("{}{}{}{}{}{}{}".format("Connection to database (", database, ") is established on ", server_name, " (", server, ")"))
          print("Connection is okay ... ")
          return connection
        except(Exception) as mssql_connection_err:
          print("Error connecting to database: ", str(mssql_connection_err))
    # End connect_db() method
# End ModifySQLServer() class
    

def lambda_handler(event=None, context=None):

    # a. instantiate class
    modify_rds_ss = ModifySQLServer()
    
    # b. modify "corestating instance"
    # i. get reference to the correct instances to be modified and deleted, respectively, by using date/time created
    
    region_name = "region_name"  # "us-west-2" or "us-west-1", etc.
    instance_identifier_one = "corestaging-1"
    instance_identifier_two = "corestaging-2"
    key_id = "key_id_value"
    a_name = "a_name_value"
    cname_names = ["cname_names-1", "cname_names-2"]
    cname_value = "{}{}{}{}".format(instance_identifier, ".cmfjcnivlgsc.", region_name, ".rds.amazonaws.com")
    dns_name = "dns_name."
    hosted_zone_id = "hosted_zone_id"
    secret_name = "/path_to_secret_password/on_secret_manager_dasboard/"
    stalled_db_name = "stalled_db_name"
    
    try:
        instance_identifier = modify_rds_ss.get_latest_or_oldest_created_instance_identifier(instance_identifier_one=instance_identifier_one,
                                                                                             instance_identifier_two=instance_identifier_two,
                                                                                             return_option="latest")
        previous_day_instance_identifier = modify_rds_ss.get_latest_or_oldest_created_instance_identifier(instance_identifier_one=instance_identifier_one,
                                                                                                          instance_identifier_two=instance_identifier_two,
                                                                                                          return_option="oldest")
                                                                                                         
        print("Newly created instance", instance_identifier)
        print("Previously created instance", previous_day_instance_identifier)
        
         # ii then modify
        modify_rds_ss.modify_corestaging_instance_after_restore(target_instance_identifier=instance_identifier, key_id=key_id)
        print("")
        print(instance_identifier, "auto-backup is being disabled.")
        print(instance_identifier, "performance insights are being enabled.")
        print(instance_identifier, "cloudwatch logs export is being enabled.")
        print("")
   

        # c. update Route53 with private ip and cname values
        a_name = a_name
        cname_names = cname_names
        cname_value = cname_value
        dns_name = dns_name
        hosted_zone_id =  hosted_zone_id
        endpoint = cname_value
        ttl = 300
        check_ip_details = True
        modify_rds_ss.update_corestaging_private_ip_address_and_cname(a_name=a_name, dns_name=dns_name, hosted_zone_id=hosted_zone_id,
                                                                      ttl=ttl, endpoint=endpoint, check_ip_details=check_ip_details, cname_names=cname_names)
                                                            
    
        # d. sleep a little while (6 secs), get corestaging credentials & connect/login to stop the stalled "restoring state" of  "stalled_db_name"
        # DISABLE FOR NOW (As at July-24-2020, by setting run_stored_procedure to False)
        # ENABLE BACK (i.e. set run_stored_procedure to True) - Only if the  stalled "restoring state" is noticed or occured again
        run_stored_procedure = False
        if run_stored_procedure:
            # i. sleep
            sleep(6)
            # ii. get credentials
            region_name = region_name
            session = Session(region_name=region_name)
            client = session.client(service_name='secretsmanager', region_name=region_name)
            secret_name = secret_name
            get_secret_value_response = client.get_secret_value(SecretId=secret_name)
            if 'SecretString' in get_secret_value_response:
                try:
                    secret = loads(get_secret_value_response['SecretString'])
                    user = secret.get("username")
                    password = secret.get("password")
                    database = "master"
                    # iii. connect/login and issue command to stop "stalled restore"
                    connection = modify_rds_ss.connect_db(server=endpoint, user=user, password=password, database=database, server_name=instance_identifier)
                    mssql_query = "USE msdb; EXEC dbo.rds_complete_restore @db_name=" + stalled_db_name + ";"
                    cursor = connection.cursor()
                    cursor.execute(mssql_query)
                except(Exception) as error:
                    print(error)
                else:
                    print("Successfully stopped 'stalled restore' on:", instance_identifier)
                    
                    
        # d. now delete the previous day instance (with previous_day_instance_identifier)
        #    but ensure no instance is deleted if only one corestaging exist (i.e.: instance_identifier != previous_day_instance_identifier)
        delete = True
        if delete and (instance_identifier != previous_day_instance_identifier):
            print("")
            print("Deleting corestaging SQL Server ...")
            target_instance_identifier = previous_day_instance_identifier
            
            if target_instance_identifier:
                try:
                    modify_rds_ss.delete_instance(instance_identifier=target_instance_identifier)
                    print("target_instance_identifier: ", target_instance_identifier)
                except(ClientError) as client_error:
                    print(client_error)
                    
    except(ClientError) as client_error:
        print(client_error)
            
    return "Lamda function is successfully is invoked."