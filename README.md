# README #

This README summaries the contents of this repository.

### What is this repository for? ###

* This repo contains source codes for the migration of SQL Server to RDS SQL Server.
* Version 1.0

### Cloud Formation (CF) Template ###
* Cloud formation template for the deployment of (a) RDS SQL Server and (b) Secret Manager.

### Python Scripts ###
* Credentials script for obtaining credentials of RDS SQL Server.
* Migration script for migrating SQL Server to RDS SQL Server via native restore.
* Specifications script for extracting details of a list of SQL Servers.
* Validation script for validating the source and target databases.
* Staging scripts (create and modify) for nightly staging of Primary RDS SQL Server.



### Repo usage ###

* Database migration
* Cloud Formation (CF) deployment: Deploy CF template on AWS console
* Python script seployment: **Deploy script via Lambda or AWS EC2**

### Contribution guidelines ###

* Create branch
* Add codes
* Submit pull request for code review

### Who do I talk to? ###

* Repo owner or admin

---
 * [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)