# ******************************************************************************************************************
#
#  Copyright © 2020 - present. Codertester.  All Rights Reserved.
#
#  This file implements ValidateSQLServer() class for:
#
#    a) Connecting/disconnecting to SQL Server with "pymssql" connector/driver library.
#    b) Running queries against SQL Server cluster(s) to obtain:
#       i)  Statistics of source and target databases after migration
#       ii) Validating the source and target databases
#
#    Note:
#       a) Assuming Python v3.6, pymssql can be installed as: sudo python3.6 -m pip install pymssql
#       b) GitHub repo of pymssql: https://github.com/pymssql/pymssql
# ******************************************************************************************************************

import sys
import pymssql
from pprint import pprint

class ValidateSQLServer():
    
    """
    A class for connecting to and validating SQL Server databases.
    """
      
    def __init__(self, redirect_stdout_to_file=None, output_filename=None):
        print()
        print("Initiating ValidateSQLServer Engine...")
     # End  __init__() method
  
    def connect_db(self, server=None, user=None, password=None,  database=None, server_name=None, output_filename=None):
        "A method for initiating database connection"
        try:
          connection = pymssql.connect(server=server, user=user, password=password, database=database, autocommit=True)
          print("{}{}{}{}{}{}{}{}".format("Connection to database (", database, ") is established on ", server_name, " (", server, ")", " Server."))
          print("Connection is okay ... ")
          return connection
        except(Exception) as mssql_connection_err:
          print("Error connecting to database: ", str(mssql_connection_err))
    # End  connect_db() method
    
    def query_db(self, connection=None, mssql_query=None):
        cursor = connection.cursor()
        cursor.execute(mssql_query)
        output_data = cursor.fetchall()
        return output_data
    # End query_db() method
    
    def separator_line(self):
        print("===========================================================================")
    # End separator_line() method
    
    def demarcation_lines_and_spaces(self):
        self.separator_line()
        self.separator_line()
        print("")
        print("")
    # End demarcation_lines_and_spaces() method
    
    def get_credentials(self):
        return {
            "user_source" : "my_username_source",
            "password_source" : "my_password_source",
            "user_target" : "my_username_target",
            "password_target" : "my_password_target",
            "database_name" : "master"
        }
    # End get_credentials() method
    
    def get_db_stats(self, connection=None, sql_server_name=None):
        # --  1. database name and disk summary statistics
        mssql_query = """
                USE MASTER
                SELECT  [Database Name] = SYS.DATABASES.NAME,
                		[Database Size (GB) ] = CAST(SUM(size) * 8.0/1024000 AS DECIMAL(12, 6))
                FROM SYS.DATABASES
                JOIN SYS.MASTER_FILES
                ON  SYS.DATABASES.DATABASE_ID=SYS.MASTER_FILES.DATABASE_ID
                AND (SYS.DATABASES.NAME != 'master')
                AND (SYS.DATABASES.NAME != 'model')
                AND (SYS.DATABASES.NAME != 'msdb')
                AND (SYS.DATABASES.NAME != 'tempdb')
                AND (SYS.DATABASES.NAME != 'rdsadmin')
                GROUP BY SYS.DATABASES.NAME
                ORDER BY SYS.DATABASES.NAME
            """
            
        size = self.query_db(connection=connection, mssql_query=mssql_query)

        database_list = ["db_name_one", "db_name_two", "db_name_one_three", "db_name_four", "db_name_five", "db_name_et_all"]
        count = []
        for index, path in enumerate(database_list):
            mssql_query = "{}{}{}".format("USE ",  database_list[index],
                            """ SELECT  [Database Name] = (SELECT DB_NAME()),
                                        [Number of Tables] = (SELECT COUNT(*) FROM SYS.TABLES),
                            		    [Number of Views] = (SELECT COUNT(*) FROM SYS.VIEWS),
                            		    [Number of Procedures] = (SELECT COUNT(*) FROM SYS.PROCEDURES),
                            		    [Number of Triggers] = (SELECT COUNT(*) FROM SYS.TRIGGERS),
                            		    [Number of Indexes] = (SELECT COUNT(*) FROM SYS.INDEXES),
                            		    [Number of Spatial Indexes] = (SELECT COUNT(*) FROM SYS.SPATIAL_INDEXES),
                            		    [Number of Synonyms] = (SELECT COUNT(*) FROM SYS.SYNONYMS)
                                """)
            cnt = self.query_db(connection=connection, mssql_query=mssql_query)
            count.append(cnt)

        return {"sql_server_name": sql_server_name, "DB Sizes (GB)": size, "Count of Objects": count}
    # End get_db_stats() method
        
    def validate_db(self, access_mssql=None, stat=None, in_production=None):
        self.separator_line()
        self.separator_line()
        print("")
        print("Validating ....")
        print("")
        
        self.separator_line()
        print("1.) Databases Statistics: Target")
        self.separator_line()
        print("Obj:", "Tables, Views, Procedures, Triggers, Indexes, Spatial Indexes, Synonyms, respectively")
        self.separator_line()
        pprint(stat[0])
        self.separator_line()
        self.separator_line()
        print("")
        
        self.separator_line()
        print("2.) Databases Statistics: Source")
        self.separator_line()
        print("Obj:", "Tables, Views, Procedures, Triggers, Indexes, Spatial Indexes, Synonyms, respectively")
        self.separator_line()
        pprint(stat[1])
        self.separator_line()
        self.separator_line()
        print("")
        
        size_stat_target = (stat[0].get('DB Sizes (GB)'))
        size_stat_source = (stat[1].get('DB Sizes (GB)'))
        count_obj_stat_target = (stat[0].get('Count of Objects'))
        count_obj_stat_source = (stat[1].get('Count of Objects'))
        
        # one: confirm db sizes
        confirm_db_size = []
        confirm_obj_count = []
        for index, item in enumerate(size_stat_target):
            confirm_db_size.append(size_stat_target[index][1] == size_stat_source[index][1])
            
        # two: confirm objects counts
        for index, item in enumerate(count_obj_stat_target):
            confirm_obj_count.append(count_obj_stat_target[index][0][1] == count_obj_stat_source[index][0][1])
            confirm_obj_count.append(count_obj_stat_target[index][0][2] == count_obj_stat_source[index][0][2])
            confirm_obj_count.append(count_obj_stat_target[index][0][3] == count_obj_stat_source[index][0][3])
            confirm_obj_count.append(count_obj_stat_target[index][0][4] == count_obj_stat_source[index][0][4])
            confirm_obj_count.append(count_obj_stat_target[index][0][5] == count_obj_stat_source[index][0][5])
            confirm_obj_count.append(count_obj_stat_target[index][0][6] == count_obj_stat_source[index][0][6])
            confirm_obj_count.append(count_obj_stat_target[index][0][7] == count_obj_stat_source[index][0][7])
        
        set_confirm_db_size = set(confirm_db_size)
        set_confirm_obj_count = set(confirm_obj_count)
        
        if not in_production:
            # check results of all confirmations:
            # just for testing/debugging purpose when not in_production i.e. in dev environment
            print("Source & Target DBs have thesame disk size?")
            print(confirm_db_size)
            print()
            print("Source & Target DBs have the same number of objects?")
            print(confirm_obj_count)
            print()
            print("After Union: Source & Target DBs have thesame disk size?")
            print(set(confirm_db_size))
            print()
            print("After Union: Source & Target DBs have the same number of objects?")
            print(set(confirm_obj_count))
            print()
        
        final_confirmation = (set_confirm_db_size == set_confirm_obj_count == {True})
        self.separator_line()
        print("Confirm Validation.")
        self.separator_line()
        if final_confirmation:
            print("Migrated Databases Validated Successfully ? ", "Yes")
        else:
            print("Migrated Databases Validated Successfully ? ", "No")
        self.separator_line()
        print("Validation is completed...")
    # End validate_db() method
# End ValidateSQLServer() class

def main():
    """
    main function: app entry
    """
    # 0. create instance of AccessSQLServe() class
    redirect_stdout_to_file = False
    output_filename = "db_clusters_details.txt"
    access_mssql = ValidateSQLServer(redirect_stdout_to_file=redirect_stdout_to_file)
    stat = []
    in_production = True
    
    # 1. print to console (or redirecting) all stdout to output_filename, in append ('a') mode
    #    note: file is located in current working directory
    print("Collecting and writing clusters' details to file in the CWD: In Progess.....")
    print("........")
    print(" ")
    if redirect_stdout_to_file:
        sys.stdout = open(output_filename , 'a')

    # 2. define dictionary to hold key-value pairs of db servers and ip adresses
    servers_dict = {
        # endpoint/server-url or ip
        "rds-target" : "target_ip_or_url",
        "ext-source" : "source_ip_or_url"
        #"rds-db-core-3-source" : "prod-core3-migration-masterdb-replica.cmfjcnivlgsc.us-west-2.rds.amazonaws.com"
    }
    
    # 3. show/print all available db servers
    access_mssql.separator_line()
    print("CONNECTING TO THE FOLLOWING DATABASE SERVERS: ")
    for item in servers_dict:
        print("Database Server Name: {} , IP Value : {}".format(item, servers_dict[item]))
    access_mssql.separator_line()
    access_mssql.separator_line()
    print("")

    # 4. connect and get relevant information/statistics
    server_count = 0
    for item in servers_dict:
        # a. define credentials
        server = servers_dict[item]
        server_name = item
        if server == "target_ip_or_url":
            user = access_mssql.get_credentials().get("user_target")
            password = access_mssql.get_credentials().get("password_target")
        if server == "source_ip_or_url":
            user = access_mssql.get_credentials().get("user_source")
            password = access_mssql.get_credentials().get("password_source")
        database = access_mssql.get_credentials().get("database_name")
        
        # b. update server count
        server_count = server_count  + 1
        print("(", server_count , ")")
        print("=====")
        
        # c. connect to server(s) and get relevant sql server details
        connection = access_mssql.connect_db(server=server, user=user, password=password, database=database,
                                             server_name=server_name, output_filename=output_filename)
        # i. get source and target dbs statistics
        stat.append(access_mssql.get_db_stats(connection=connection, sql_server_name=server_name))
   
    # 5. finally validate dbs and print results to console or file
    access_mssql.validate_db(access_mssql=access_mssql, stat=stat, in_production=in_production)
# End main() method

# invoke app
if __name__ in ('__main__', 'app'):
    main()